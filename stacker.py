import enum
import logging
import os
import sys
import time

import matplotlib.pyplot as plt
import numpy as np
import rawpy
import scipy.fftpack
import scipy.ndimage

import astroalign
import skimage.io
from tqdm import tqdm
import sep

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# Approximate size in pixels of star
STAR_SIZE_PX = 10

astroalign.MAX_CONTROL_POINTS = 50
astroalign.MIN_MATCHES_FRACTION = 0.90
astroalign.NUM_NEAREST_NEIGHBORS = 5
astroalign.PIXEL_TOL = 5
#STAR_SIZE_PX

FFT_FILTER = True


class StackingMode(enum.Enum):
    Maximum = 1
    AlignAndSum = 2


def read_raw_file(file_path):
    if os.path.splitext(file_path)[1] == '.CR2':
        with rawpy.imread(file_path) as raw:
            img = np.array(
                raw.postprocess(no_auto_scale=True,
                                no_auto_bright=True, output_bps=16), dtype=np.float32)
            img /= (2**16-1)
    else:
        img = skimage.io.imread(file_path) / 255.0
    return img


def checkpoint_image_path(filename, suffix):
    return os.path.join('data/checkpoints/', os.path.basename(filename) + '_' + suffix + '.npz')


def average_dark_frames(dark_frame_files):
    stacked_dark_frame = None
    for file_path in dark_frame_files:
        dark_img = read_raw_file(file_path)
        if stacked_dark_frame is None:
            stacked_dark_frame = np.zeros_like(dark_img, dtype=np.float32)

        stacked_dark_frame += dark_img

    return stacked_dark_frame / len(dark_frame_files)


def remove_light_pollution(rgb: np.array, star_size_px=STAR_SIZE_PX, sky_mask_probabilities=None):
    # The filter won't work on arrays of integer types
    assert rgb.dtype in [np.float64, np.float32]

    gaussian_filter_sigma_px = 3 * star_size_px

    pollution_mask = np.zeros_like(rgb)

    if FFT_FILTER:
        for rgb_idx in range(3):
            pollution_mask[:, :, rgb_idx] = scipy.fftpack.ifft2(
                scipy.ndimage.fourier_gaussian(
                    scipy.fftpack.fft2(rgb[:, :, rgb_idx]),
                    sigma=gaussian_filter_sigma_px)).real
    else:
        for rgb_idx in range(3):
            pollution_mask[:, :, rgb_idx] = scipy.ndimage.filters.gaussian_filter(
                rgb[:, :, rgb_idx], sigma=gaussian_filter_sigma_px)

    rgb_clean = (rgb - pollution_mask).clip(min=0.0)
    return rgb_clean, pollution_mask


def estimate_sky_noise(sky_img: np.array):
    percentile = 99.9
    thresholds = [np.percentile(
        sky_img[:, :, rgb_idx].flatten(), percentile) for rgb_idx in range(3)]
    return thresholds


def plot_rgb_hist(rgb: np.array, normalise=True):
    plt.figure()
    maxval = rgb.flatten().max() if normalise else 1.0
    plt.imshow(rgb/maxval)

    plt.figure()
    for rgb_idx in range(3):
        plt.hist(rgb[:, :, rgb_idx].flatten(), 100,
                 color=['r', 'g', 'b'][rgb_idx])


def luminance(rgb):
    assert rgb.shape[2] == 3
    return (.299 * rgb[:, :, 0]) + (.587 * rgb[:, :, 1]) + (.114 * rgb[:, :, 2])


def find_sources(image):
    """Use this function instead of astroalign.find_sources for images where the background
    has already been removed
    """
    THRESHOLD = 0.01
    #THRESHOLD = np.max(image) / 10.0

    #import ipdb; ipdb.set_trace();

    sources = sep.extract(image, THRESHOLD)
    sources.sort(order="flux")
    sources = sources[::-1]
    logger.info(f"Found {len(sources)} stars.")

    # Try to provide sources uniformly around the image with the heuristics
    # 20% of the brightest
    # 20% in each image quadrant
    all_stars = np.array([[asrc["x"], asrc["y"]] for asrc in sources])

    half = astroalign.MAX_CONTROL_POINTS // 2
    eighth = astroalign.MAX_CONTROL_POINTS // 8

    # Brightest
    keep_mask = np.zeros(len(all_stars), dtype=np.bool)
    keep_mask[:half] = True
    # Horizontal distribution
    left_to_right_idx = np.argsort(all_stars[:, 0])
    keep_mask[left_to_right_idx[:eighth]] = True
    keep_mask[left_to_right_idx[::-1][:eighth]] = True
    # Vertical distribution
    top_to_bottom_idx = np.argsort(all_stars[:, 1])
    keep_mask[top_to_bottom_idx[:eighth]] = True
    keep_mask[top_to_bottom_idx[::-1][:eighth]] = True

    kept_stars = all_stars[keep_mask, :]

    # np.random.default_rng(100).shuffle(kept_stars)
    logger.info(f"Kept {len(kept_stars)} stars.")

    return kept_stars


def debug_alignment_plot(img0, img1, img_final, pos_matches, all_sources, all_targets):

    if pos_matches is not None:
        source_star_pos, target_star_pos = pos_matches
    fig, axes = plt.subplots(2, 2, figsize=(30, 20))

    ax = axes[0, 0]
    ax.imshow(img0*20, vmin=0, vmax=1.0, cmap='gray',
              interpolation='none', origin='lower')
    if pos_matches is not None:
        ax.plot(source_star_pos[:, 0],
                source_star_pos[:, 1], 'r+', markersize=10)
    ax.plot(all_sources[:, 0], all_sources[:, 1], 'ro')
    ax.set_title("Source Image")

    ax = axes[1, 0]
    ax.imshow(img1 * 20, vmin=0, vmax=1.0, cmap='gray',
              interpolation='none', origin='lower')
    if pos_matches is not None:
        for k in range(pos_matches[0].shape[0]):
            ax.plot([pos_matches[0][k, 0], pos_matches[1][k, 0]],
                    [pos_matches[0][k, 1], pos_matches[1][k, 1]], 'r-+')
        ax.plot(source_star_pos[:, 0],
                source_star_pos[:, 1], 'r+', markersize=10)
        ax.plot(target_star_pos[:, 0],
                target_star_pos[:, 1], 'g+', markersize=10)
    ax.plot(all_targets[:, 0], all_targets[:, 1], 'go')
    ax.set_title("Target Image")

    ax = axes[0, 1]
    ax.imshow(img_final * 20, vmin=0, vmax=1.0, cmap='gray',
              interpolation='none', origin='lower')
    if pos_matches is not None:
        ax.plot(target_star_pos[:, 0],
                target_star_pos[:, 1], 'b+', markersize=10)
    ax.set_title("Aligned Image")
    plt.tight_layout()
    plt.ion()
    plt.draw()
    plt.pause(0.1)
    return fig, ax


def align_using_region(source, target, sky_mask=None, debug_info=""):
    img_aligned_rgb = np.zeros_like(target)

    img_source_gray = np.mean(source, axis=2).clip(min=0.0)
    img_target_gray = np.mean(target, axis=2).clip(min=0.0)

    if sky_mask is None:
        sky_mask = np.ones_like(img_source_gray, dtype=np.uint8)

    try:
        source_all_stars = find_sources(img_source_gray * sky_mask)
        target_all_stars = find_sources(img_target_gray * sky_mask)

        transformation, pos_matches = astroalign.find_transform(
            source_all_stars, target_all_stars)

        earth_omega_rad_sec = 2*np.pi / (23*3600+56*60+4.091)
        estimated_time_interval = transformation.rotation / earth_omega_rad_sec
        logger.info(
            f"Rotation by {np.rad2deg(transformation.rotation):.3f} degrees, {estimated_time_interval:.2f} seconds of Earth rotation.\n"
            f"Scale of {transformation.scale}\n"
            f"Translation of {transformation.translation}")

    except astroalign.MaxIterError as e:
        logger.error("Failed to align, return original image")
        logger.exception(e)
        debug_alignment_plot(img_source_gray, img_target_gray, np.zeros_like(
            source), None, source_all_stars, target_all_stars)
        return np.zeros_like(source)

    for rgb_idx in range(3):
        img_aligned_rgb[:, :, rgb_idx], _ = astroalign.apply_transform(
            transformation,
            source[:, :, rgb_idx],
            target[:, :, rgb_idx])

    fig, ax = debug_alignment_plot(img_source_gray, img_target_gray, img_aligned_rgb,
                         pos_matches, source_all_stars, target_all_stars)
    fig.suptitle(debug_info)

    return img_aligned_rgb


def process_light_pollution(file_path, dark_frame=None, force_rewrite=False):
    logger.info(f"Started processing {file_path}")
    clean_img_path = checkpoint_image_path(file_path, 'cleaned')

    t0 = time.time()

    if not os.path.exists(clean_img_path) or force_rewrite:
        rgb = read_raw_file(file_path)
        if dark_frame is None:
            dark_frame = np.zeros_like(rgb)

        # Remove bias and dead pixels
        rgb = (rgb - dark_frame).clip(min=0.0)

        # Blur and remove light pollution
        rgb_clean, _ = remove_light_pollution(rgb)

        # Save output
        np.savez_compressed(clean_img_path, rgb_clean=rgb_clean)

    logger.info(f"Image clean up done in {time.time() - t0:.2f} s.")


def align_and_stack(input_files, sky_prob=None, stacking_mode=StackingMode.Maximum, pure_sky=np.s_[:, :]):

    stacked_img = None
    first_rgb_clean = None

    if sky_prob is not None:
        sky_mask = np.ones_like(sky_prob, dtype=np.uint8)
        sky_mask[sky_prob < 0.999] = 0.0

    for file_idx, file_path in enumerate(tqdm(input_files)):
        logger.info(f"Started stacking image {file_path}")
        clean_img_path = checkpoint_image_path(file_path, 'cleaned')

        if not os.path.exists(clean_img_path):
            raise RuntimeError("Can't open pre-processed file")

        logger.info(f"Re-using clean image {clean_img_path}")
        rgb_clean = np.load(clean_img_path)['rgb_clean']

        # Initialise output images on first iteration
        if stacked_img is None:
            stacked_img = np.zeros_like(rgb_clean)
            if sky_prob is None:
                logger.info(
                    "Not using a sky mask, assuming entire image is valid sky.")
                sky_mask = np.ones_like(rgb_clean[:,:,0], dtype=np.uint8)

        if stacking_mode == StackingMode.AlignAndSum:
            # Sum
            if file_idx > 0:
                aligned_img_path = checkpoint_image_path(file_path, 'aligned')
                t1 = time.time()
                rgb_clean_aligned = align_using_region(
                    rgb_clean, first_rgb_clean, sky_mask=sky_mask)

                # FIXME second time to fix lens creep
                rgb_clean_aligned = align_using_region(
                    rgb_clean_aligned, first_rgb_clean, sky_mask=sky_mask)

                logger.info(f"Aligning done in {time.time() - t1:.2f} s.")
                np.savez_compressed(
                    aligned_img_path, rgb_clean_aligned=rgb_clean_aligned)
            else:
                # Can't align when we have no reference
                rgb_clean_aligned = rgb_clean
                first_rgb_clean = rgb_clean

            stacked_img += rgb_clean_aligned

            # plt.figure(2)
            debug_img = (stacked_img / stacked_img.max()
                         * 10.0) / (file_idx + 1)
            skimage.io.imsave(
                f"data/output/stacked_img{file_idx:04d}.png", debug_img)
            # f"data/output/stacked_img{file_idx:04d}.png", debug_img[2800:3200, 4000:4400, :])
            # plt.imshow(debug_img[2800:3200, 4150:4300, :])
            # plt.pause(0.1)

        elif stacking_mode == StackingMode.Maximum:
            # Max
            stacked_img = np.maximum(rgb_clean, stacked_img)
        else:
            raise ValueError("Stacking mode not supported")

    if stacking_mode == StackingMode.AlignAndSum:
        stacked_img /= len(input_files)

    np.save(os.path.join("data/checkpoints", "stacked_img"), stacked_img)


    return stacked_img
